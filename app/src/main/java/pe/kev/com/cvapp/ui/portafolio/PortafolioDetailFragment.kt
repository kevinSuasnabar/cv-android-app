package pe.kev.com.cvapp.ui.portafolio

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import pe.kev.com.cvapp.R
import pe.kev.com.cvapp.data.model.PortafolioItem
import com.bumptech.glide.Glide
import pe.kev.com.cvapp.databinding.FragmentPortafolioDetailBinding


class PortafolioDetailFragment : BottomSheetDialogFragment() {

    private lateinit var binding: FragmentPortafolioDetailBinding
    private val args by navArgs<PortafolioDetailFragmentArgs>()//obtenieido datos pasados desde el otro fragment
    private lateinit var item:PortafolioItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        args?.let {
            item=PortafolioItem(args.imgProyect,args.proyectName,args.descProyect)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_portafolio_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentPortafolioDetailBinding.bind(view)
        loadPortafolioData(item)

    }

    fun loadPortafolioData(portafolioItem: PortafolioItem) {
        Glide.with(requireContext()).load(portafolioItem.image).into(binding.imgPortafolioDetail)
        binding.txtDescription.text=item.description
        binding.txtProyectName.text=item.title
    }

}