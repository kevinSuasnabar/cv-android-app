package pe.kev.com.cvapp.ui.portafolio.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import pe.kev.com.cvapp.core.BaseViewHolder
import pe.kev.com.cvapp.data.model.MenuItem
import pe.kev.com.cvapp.data.model.PortafolioItem
import pe.kev.com.cvapp.databinding.ItemPortafolioBinding
import pe.kev.com.cvapp.ui.sidemenu.adapters.MenuAdapter
import com.bumptech.glide.Glide

class PortafolioAdapter(
    private val context: Context,
    private val portafolioItems: List<PortafolioItem>,
    private val itemClickLister: OnPortafolioClickListener
) : RecyclerView.Adapter<BaseViewHolder<*>>() {

    interface OnPortafolioClickListener{
        fun OnPortafolioItemClick(item: PortafolioItem, position: Int)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val itemBinding =
            ItemPortafolioBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val holder = PortafolioViewHolder(itemBinding, parent.context)
        /**el .root nos indica que tomara toda la vista inflada que tiene itemBinding*/
        itemBinding.root.setOnClickListener{
            /**.takeIf valida que se tome una posicion y si no devuelve un -1*/
            val position = holder.adapterPosition.takeIf { it != DiffUtil.DiffResult.NO_POSITION }
                ?:return@setOnClickListener // si no se obtiene una posicion solo me devuelve el evento sin ninguna posicion

            /**Evento click sobre una pelicula*/
            itemClickLister.OnPortafolioItemClick(portafolioItems[position],position)
        }
        return holder
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when (holder) {
            is PortafolioViewHolder -> holder.bind(portafolioItems[position])
        }
    }

    override fun getItemCount(): Int {
        return portafolioItems.size
    }

    private inner class PortafolioViewHolder(
        val binding: ItemPortafolioBinding,
        val context: Context
    ) : BaseViewHolder<PortafolioItem>(binding.root) {
        override fun bind(item: PortafolioItem) {
            Glide.with(context).load(item.image).into(binding.imgPortafolio)
        }


    }


}