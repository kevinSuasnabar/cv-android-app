package pe.kev.com.cvapp.data.model

import com.google.gson.annotations.SerializedName

data class MenuItem(
    @SerializedName("icon") val icon: Int = -1,
    @SerializedName("code") var isSelected: Boolean = false,
    @SerializedName("code") val code: Int = -1
)