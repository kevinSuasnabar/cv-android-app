package pe.kev.com.cvapp.data.model

import com.google.gson.annotations.SerializedName

data class PortafolioItem(
    @SerializedName("image") val image: Int = -1,
    @SerializedName("title") val title: String = "",
    @SerializedName("description") val description: String = ""
)