package pe.kev.com.cvapp.config

import pe.kev.com.cvapp.data.model.MenuItem
import pe.kev.com.cvapp.R

object MenuConstants {
    const val HOME_FRAGMENT_CODE = 0
    const val CV_FRAGMENT_CODE = 1
    const val TECH_FRAGMENT_CODE = 2
    const val PORTAFOLIO_FRAGMENT_CODE = 3

    fun getMenuList(): List<MenuItem> {
        val list = mutableListOf<MenuItem>(
            MenuItem(R.drawable.ic_baseline_home_24, true, HOME_FRAGMENT_CODE),
            MenuItem(R.drawable.ic_baseline_school_24, false, CV_FRAGMENT_CODE),
            MenuItem(R.drawable.ic_baseline_desktop_mac_24, false, PORTAFOLIO_FRAGMENT_CODE),
            MenuItem(R.drawable.ic_baseline_dashboard_24, false, TECH_FRAGMENT_CODE)
        )
        return list
    }


}