package pe.kev.com.cvapp.data.model

import com.google.gson.annotations.SerializedName

data class TechnologyItem(
    @SerializedName("name") val name: String = "",
    @SerializedName("description") val description: String = "",
    @SerializedName("image") val image: Int = -1
)