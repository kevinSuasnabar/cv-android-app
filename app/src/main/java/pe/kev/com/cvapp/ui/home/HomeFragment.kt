package pe.kev.com.cvapp.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import pe.kev.com.cvapp.R
import pe.kev.com.cvapp.databinding.FragmentHomeBinding

class HomeFragment : Fragment(R.layout.fragment_home) {

    private lateinit var binding: FragmentHomeBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentHomeBinding.bind(view)
    }


}