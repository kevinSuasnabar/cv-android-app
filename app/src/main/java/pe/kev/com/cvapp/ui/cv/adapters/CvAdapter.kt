package pe.kev.com.cvapp.ui.cv.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pe.kev.com.cvapp.core.BaseViewHolder
import pe.kev.com.cvapp.data.model.CvItem
import pe.kev.com.cvapp.databinding.ItemCvBinding

class CvAdapter(private val context: Context, private val cvList: List<CvItem>) :
    RecyclerView.Adapter<BaseViewHolder<*>>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val itemBinding = ItemCvBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        val holder = CvViewHolder(itemBinding,parent.context)
        return holder
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when(holder){
            is CvViewHolder -> holder.bind(cvList[position])
        }
    }

    override fun getItemCount(): Int {
        return cvList.size
    }

    private inner class CvViewHolder(val binding: ItemCvBinding, val context: Context) : BaseViewHolder<CvItem>(binding.root) {

        override fun bind(item: CvItem) {
            binding.txtDescription.text=item.description
            binding.txtTitle.text=item.title
        }

    }

}