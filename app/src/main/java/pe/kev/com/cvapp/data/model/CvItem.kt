package pe.kev.com.cvapp.data.model

import com.google.gson.annotations.SerializedName

data class CvItem(
    @SerializedName("title") val title: String = "",
    @SerializedName("description") val description: String = ""
    )