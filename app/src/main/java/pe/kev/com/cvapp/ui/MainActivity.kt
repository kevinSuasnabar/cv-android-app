package pe.kev.com.cvapp.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.navigation.findNavController
import pe.kev.com.cvapp.R
import pe.kev.com.cvapp.config.MenuConstants
import pe.kev.com.cvapp.data.model.MenuItem
import pe.kev.com.cvapp.databinding.ActivityMainBinding
import pe.kev.com.cvapp.ui.sidemenu.adapters.MenuAdapter

class MainActivity : AppCompatActivity(), MenuAdapter.OnMenuClickListener {

    private lateinit var binding: ActivityMainBinding
    private lateinit var menuAdapter: MenuAdapter
    var menuItems = MenuConstants.getMenuList()
    var itemSelectedPos: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setupMenu()
    }


    fun setupMenu() {
        Log.d("MenuItems", menuItems.toString())
        menuAdapter = MenuAdapter(this, menuItems, this)
        binding.rvSidebar.adapter = menuAdapter
    }

    override fun OnMenuItemClick(item: MenuItem, position: Int) {

        when (position) {
            MenuConstants.CV_FRAGMENT_CODE -> findNavController(R.id.nav_host_fragment).navigate(R.id.cvFragment)
            MenuConstants.HOME_FRAGMENT_CODE -> findNavController(R.id.nav_host_fragment).navigate(R.id.homeFragment)
            MenuConstants.TECH_FRAGMENT_CODE -> findNavController(R.id.nav_host_fragment).navigate(R.id.technologyFragment)
            MenuConstants.PORTAFOLIO_FRAGMENT_CODE -> findNavController(R.id.nav_host_fragment).navigate(
                R.id.portafolioFragment
            )
        }

        //selected menu item color
        menuItems[itemSelectedPos].isSelected=false
        menuItems[position].isSelected = true
        itemSelectedPos = position
        menuAdapter.notifyDataSetChanged()


    }


}