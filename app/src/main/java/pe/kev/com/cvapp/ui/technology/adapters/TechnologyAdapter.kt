package pe.kev.com.cvapp.ui.technology.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pe.kev.com.cvapp.core.BaseViewHolder
import pe.kev.com.cvapp.data.model.TechnologyItem
import pe.kev.com.cvapp.databinding.ItemTecnologyBinding

class TechnologyAdapter(
    private val context: Context,
    private val technologyItems: List<TechnologyItem>
) :
    RecyclerView.Adapter<BaseViewHolder<*>>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val itemBinding =
            ItemTecnologyBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val holder = TechnologyViewHolder(itemBinding, parent.context)
        return holder
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when (holder) {
            is TechnologyViewHolder -> holder.bind(technologyItems[position])
        }
    }

    override fun getItemCount(): Int {
        return technologyItems.size
    }

    private inner class TechnologyViewHolder(
        val binding: ItemTecnologyBinding,
        val context: Context
    ) : BaseViewHolder<TechnologyItem>(binding.root) {
        override fun bind(item: TechnologyItem) {
            binding.txtTechnologyDesc.text=item.description
            binding.txtTechnologyTitle.text=item.name
            binding.imgTechnology.setImageResource(item.image)
        }


    }

}