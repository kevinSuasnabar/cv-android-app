package pe.kev.com.cvapp.ui.sidemenu.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import pe.kev.com.cvapp.core.BaseViewHolder
import pe.kev.com.cvapp.data.model.MenuItem
import pe.kev.com.cvapp.databinding.ItemMenuBinding

class MenuAdapter(
    private val context: Context,
    private val menuItems: List<MenuItem>,
    private val itemClickLister:OnMenuClickListener
) : RecyclerView.Adapter<BaseViewHolder<*>>() {

    interface OnMenuClickListener{
        fun OnMenuItemClick(item: MenuItem,position: Int)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val itemBinding =
            ItemMenuBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val holder = MenuViewHolder(itemBinding, parent.context)


        /**el .root nos indica que tomara toda la vista inflada que tiene itemBinding*/
        itemBinding.root.setOnClickListener{
            /**.takeIf valida que se tome una posicion y si no devuelve un -1*/
            val position = holder.adapterPosition.takeIf { it != DiffUtil.DiffResult.NO_POSITION }
                ?:return@setOnClickListener // si no se obtiene una posicion solo me devuelve el evento sin ninguna posicion

            /**Evento click sobre una pelicula*/
            itemClickLister.OnMenuItemClick(menuItems[position],position)
        }
        return holder
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when (holder) {
            is MenuViewHolder -> holder.bind(menuItems[position])
        }
    }

    override fun getItemCount(): Int {
        return menuItems.size
    }

    private inner class MenuViewHolder(
        val binding: ItemMenuBinding,
        val context: Context
    ) : BaseViewHolder<MenuItem>(binding.root) {
        override fun bind(item: MenuItem) {
            binding.imgMenuIcon.setImageResource(item.icon)
            if (item.isSelected) {
                binding.imgMenuSelected.visibility = View.VISIBLE
            } else {
                binding.imgMenuSelected.visibility = View.GONE
            }
        }


    }


}