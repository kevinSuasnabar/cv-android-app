package pe.kev.com.cvapp.ui.technology

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import pe.kev.com.cvapp.R
import pe.kev.com.cvapp.data.model.TechnologyItem
import pe.kev.com.cvapp.databinding.FragmentTechnologyBinding
import pe.kev.com.cvapp.ui.technology.adapters.TechnologyAdapter


class TechnologyFragment : Fragment(R.layout.fragment_technology) {

    private lateinit var binding: FragmentTechnologyBinding
    private lateinit var technologyAdapter: TechnologyAdapter

    //Lista temporal
    private val techItems = mutableListOf(
        TechnologyItem(
            "Angular",
            "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English",
            R.drawable.profile
        ),
        TechnologyItem(
            "Spring Boot",
            "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English",
            R.drawable.profile_3
        ), TechnologyItem(
            "NodeJs",
            "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English",
            R.drawable.profile_2
        )
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentTechnologyBinding.bind(view)

        technologyAdapter = TechnologyAdapter(requireContext(),techItems)
        binding.rvTecnology.adapter= technologyAdapter
    }
}