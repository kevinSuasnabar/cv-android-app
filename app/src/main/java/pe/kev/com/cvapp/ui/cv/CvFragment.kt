package pe.kev.com.cvapp.ui.cv

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import pe.kev.com.cvapp.R
import pe.kev.com.cvapp.data.model.CvItem
import pe.kev.com.cvapp.databinding.FragmentCvBinding
import pe.kev.com.cvapp.ui.cv.adapters.CvAdapter


class CvFragment : Fragment(R.layout.fragment_cv) {
    private lateinit var binding: FragmentCvBinding
    private lateinit var cvAdapter: CvAdapter


    //Lista temporal
    private val cvItems = mutableListOf(
        CvItem("Titulo 1", "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English"),
        CvItem("Titulo 2", "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English"),
        CvItem("Titulo 3", "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English"),
        CvItem("Titulo 4", "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English"),
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentCvBinding.bind(view)

        cvAdapter = CvAdapter(requireContext(),cvItems)
        binding.rvCv.adapter= cvAdapter
    }
}