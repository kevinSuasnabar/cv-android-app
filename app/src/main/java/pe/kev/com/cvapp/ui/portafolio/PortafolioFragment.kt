package pe.kev.com.cvapp.ui.portafolio

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.fragment.app.FragmentManager
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import pe.kev.com.cvapp.R
import pe.kev.com.cvapp.data.model.MenuItem
import pe.kev.com.cvapp.data.model.PortafolioItem
import pe.kev.com.cvapp.databinding.FragmentPortafolioBinding
import pe.kev.com.cvapp.ui.portafolio.adapters.PortafolioAdapter

class PortafolioFragment : Fragment(R.layout.fragment_portafolio),
    PortafolioAdapter.OnPortafolioClickListener {

    private lateinit var binding: FragmentPortafolioBinding
    private lateinit var portafolioAdapter: PortafolioAdapter

    //Lista temporal
    private val portafolioItems = mutableListOf(
        PortafolioItem(
            R.drawable.profile,
            "1",
            "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English"
        ),
        PortafolioItem(
            R.drawable.profile_3,
            "2",
            "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English"
        ), PortafolioItem(
            R.drawable.profile_2,
            "3",
            "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English"

        ),
        PortafolioItem(
            R.drawable.profile,
            "4",
            "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English"

        )
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentPortafolioBinding.bind(view)
        portafolioAdapter = PortafolioAdapter(requireContext(), portafolioItems, this)
        binding.rvPortafolio.adapter = portafolioAdapter
    }

    override fun OnPortafolioItemClick(item: PortafolioItem, position: Int) {
        var actions =
            PortafolioFragmentDirections.actionPortafolioFragmentToPortafolioDetailFragment(
                item.title,
                item.image,
                item.description
            )
        findNavController().navigate(actions)

    }


}